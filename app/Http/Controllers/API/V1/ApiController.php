<?php

/**
 * Created by PhpStorm.
 * User: sebas
 * Date: 16/03/19
 * Time: 16:29
 */

namespace App\Http\Controllers\API\V1;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Response;

class ApiController extends Controller
{
    /**
     * API response function to be used by all endpoints
     * @param array $body
     * @param null $message
     * @param bool $success
     * @param int $status
     * @return JsonResponse response
     */
    public function api_response($body, $success = true, $message = null, $status = 200){
        if(is_object($body)) {
            $body = (array)$body;
            // Protected properties handling
            $prefix = chr(0).'*'.chr(0);

            if(isset($body[$prefix.'data'])){
                $body = json_decode($body[$prefix.'data'], true)['data'];
            }

            $count = count($body);
            $body = ['data' => $body];

        } elseif(isset($body['data'])){

            $count = count($body['data']);

        }else{

            $count = count($body);
            $body = ['data' => $body];

        }

        $payload = [
            'result' => [
                'success' => $success,
                'message' => $message,
                'count' => $count,],
            'response_payload' => $body
        ];
        return new JsonResponse($payload, $status);
    }
}