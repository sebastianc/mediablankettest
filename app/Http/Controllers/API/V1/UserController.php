<?php

namespace App\Http\Controllers\API\V1;

use App\User;
use App\Http\Controllers\Controller as Controller;
use App\Services\MediaBlanket\SupaData;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Middleware;
use GuzzleHttp\RequestOptions as RequestOptions;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;

class UserController extends ApiController
{

    public function getUsers()
    {
        $users = User::orderBy('id', 'asc')->paginate(15);
        return parent::api_response($users->toArray(), true, ['return' => 'all users']);
    }

    function getById($id)
    {
        $user = User::find($id);
        if ($user) {
            return parent::api_response($user->toArray(), true, ['return' => 'User with id ' . $id], 200);
        } else {
            return parent::api_response([], false, ['error' => 'User not found'], 404);
        }
    }

    public function postLeadsToMb()
    {
        $users = User::orderBy('sub_id', 'asc')->get();
        foreach ($users as $member) {
            $member->sub_id = $member->id;
            unset($member->id);
        }

        // For demo purposes we will try to submit the first user
        $user =  $users[0]->toArray();

        $mbData = (new SupaData())->post('', $user);

        /*
         * Debugging can be easily achieved by switching the 'debugging_handler' to true in the SupaData service
         * With native Guzzle debugging we can see which data is actually sent
        */

        if (isset($mbData->status) || isset($mbData->message)) {
            if( $mbData->status == 'Accepted' || $mbData->status == 'Declined' ) {
                return parent::api_response((array)$mbData, true, ['return' => 'Successfully posted users to Mb Api'], 200);
            } else {
                return parent::api_response((array)$mbData, false, ['return' => 'Validation errors from Mb Api'], 403);
            }
        } else {
            return parent::api_response((array)$mbData, false, ['error' => 'Got a failed response from Mb Api, service could be down'], 500);
        }

    }

}