<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'test_lead', 'aff_id', 'aff_password', 'referring_website', 'loan_amount', 'loan_purpose', 'loan_term', 'guarantor',
        'title', 'first_name', 'middle_name', 'last_name', 'gender', 'date_of_birth', 'marital_status', 'number_of_dependents',
        'house_number', 'house_name', 'flat_number', 'street_name', 'city', 'county', 'post_code', 'residential_status',
        'address_move_in_date', 'mobile_number', 'home_number', 'work_number', 'mobile_phone_type', 'email_address',
        'employment_status', 'payment_frequency', 'payment_method', 'monthly_income', 'next_pay_date', 'following_pay_date',
        'job_title', 'employer_name', 'employer_industry', 'employment_start_date', 'expenditure_housing', 'expenditure_credit',
        'expenditure_transport', 'expenditure_food', 'expenditure_utilities', 'expenditure_other', 'bank_name', 'bank_account_number',
        'bank_sort_code', 'bank_card_type', 'consent_email_sms', 'consent_email', 'consent_sms', 'consent_call', 'consent_credit_search',
        'consent_financial', 'user_agent', 'ip_address'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    /**
     * General Mobile number validation
     *
     * @return bool
     */
    public function isMobileNumber()
    {
        if (strpos($this->mobile_number, "+447")
            !== false ||
            substr($this->mobile_number, 0, 2) == "07") {
            return true;
        }

        return false;
    }

    /**
     * Uk Phones mobile number validation
     *
     * @return bool
     */
    public function isUkMobile()
    {
        $regex = '/^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$/';
        return (preg_match($regex, $this->mobile_number));
    }

    /**
     * Uk Phones home number validation
     *
     * @return bool
     */
    public function isUkHome()
    {
        $regex = '/^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$/';
        return (preg_match($regex, $this->home_number));
    }

    /**
     * Uk Phones work number validation
     *
     * @return bool
     */
    public function isUkWork()
    {
        $regex = '/^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$/';
        return (preg_match($regex, $this->work_number));
    }

    /**
     * Email validation
     *
     * @return bool
     */
    public function isValidEmail(){
        return filter_var($this->email_address, FILTER_VALIDATE_EMAIL) !== false;
    }

    /**
     * Date validation checks date of birth is in the required format (DD-MM-YYYY)
     *
     * @return bool
     */
    public function isDateOfBirthRight() {
        $matches = array();
        $pattern = '/^([0-9]{1,2})-([0-9]{1,2})-([0-9]{4})$/';
        if (!preg_match($pattern, $this->date_of_birth, $matches) || !checkdate($matches[2], $matches[1], $matches[3])) {
            $this->date_of_birth = date("d-m-Y", strtotime($this->date_of_birth));
        }
        return true;
    }

    /**
     * Date validation checks address moving date is in the required format (DD-MM-YYYY)
     *
     * @return bool
     */
    public function isAddressMovingDateRight() {
        $matches = array();
        $pattern = '/^([0-9]{1,2})-([0-9]{1,2})-([0-9]{4})$/';
        if (!preg_match($pattern, $this->address_move_in_date, $matches) || !checkdate($matches[2], $matches[1], $matches[3])) {
            $this->address_move_in_date = date("d-m-Y", strtotime($this->address_move_in_date));
        }
        return true;
    }

    /**
     * Date validation checks if next pay date is in the required format (DD-MM-YYYY)
     *
     * @return bool
     */
    public function isNextPayDateRight() {
        $matches = array();
        $pattern = '/^([0-9]{1,2})-([0-9]{1,2})-([0-9]{4})$/';
        if (!preg_match($pattern, $this->next_pay_date, $matches) || !checkdate($matches[2], $matches[1], $matches[3])) {
            $this->next_pay_date = date("d-m-Y", strtotime($this->next_pay_date));
        }
        if(date('d-m-Y',strtotime(date("d-m-Y", time()) . " + 45 day")) < date("d-m-Y", strtotime($this->next_pay_date))){
            $this->next_pay_date = date("d-m-Y",strtotime(date("d-m-Y", time()) . " + 30 day"));
        }
        return true;
    }

    /**
     * Date validation checks if following pay date is in the required format (DD-MM-YYYY)
     *
     * @return bool
     */
    public function isFollowingPayDateRight() {
        $matches = array();
        $pattern = '/^([0-9]{1,2})-([0-9]{1,2})-([0-9]{4})$/';
        if (!preg_match($pattern, $this->following_pay_date, $matches) || !checkdate($matches[2], $matches[1], $matches[3])) {
            $this->following_pay_date = date("d-m-Y", strtotime($this->following_pay_date));
        }
        if((date('d-m-Y',strtotime($this->following_pay_date)) < date("d-m-Y", strtotime($this->next_pay_date)))
            || (date('d-m-Y',strtotime(date("d-m-Y", time()) . " + 90 day")) < date("d-m-Y", strtotime($this->following_pay_date)))
            || (date("d-m-Y", strtotime($this->following_pay_date) < date('d-m-Y',strtotime(date("d-m-Y", time())))))
        ){
            $this->following_pay_date = date("d-m-Y",strtotime(date("d-m-Y", time()) . " + 50 day"));
        }
        return true;
    }

    /**
     * Date validation checks if employment start date is in the required format (DD-MM-YYYY)
     *
     * @return bool
     */
    public function isEmploymentStartDateRight() {
        $matches = array();
        $pattern = '/^([0-9]{1,2})-([0-9]{1,2})-([0-9]{4})$/';
        if (!preg_match($pattern, $this->employment_start_date, $matches) || !checkdate($matches[2], $matches[1], $matches[3])) {
            $this->employment_start_date = date("d-m-Y", strtotime($this->employment_start_date));
        }
        return true;
    }
}
