<?php

namespace App\Services\MediaBlanket;

use App\Services\HttpService;

class SupaData extends HttpService
{
    public function __construct()
    {
        $this->config = [
            'base_uri' => (env("APP_ENV") === "production") ? config('services.mb-service.production') : config('services.mb-service.development'),
            'headers' => [
            //'Accept' => 'application/json' ,
            'Content-Type' => 'application/json'
            ],
            'debugging_handler' => false
        ];
        parent::__construct();
    }

}