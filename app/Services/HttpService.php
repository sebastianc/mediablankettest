<?php

/**
 * Created by PhpStorm.
 * User: sebas
 * Date: 16/03/19
 * Time: 16:29
 */

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Middleware;
use Illuminate\Http\Response;
use stdClass;

class HttpService
{

    /**
     * The configuration for the HTTP client.
     *
     * @var array
     */
    protected $config = [];

    /**
     * The HTTP client instance.
     *
     * @var Client
     */
    private $client;

    /**
     * Create a new class instance.
     *
     * @return void
     */
    public function __construct()
    {
        abort_unless(
            array_key_exists('base_uri', $this->config),
            Response::HTTP_INTERNAL_SERVER_ERROR,
            'The configuration is missing [base_uri] key.'
        );
        $this->client = new Client(['base_uri' => $this->config['base_uri'],'headers' => $this->config['headers']]);

    }

    /**
     * Make a GET HTTP request.
     *
     * @param string $endPoint
     * @param array $data
     *
     * @return stdClass
     */
    public function get(string $endPoint = '', array $data = []): stdClass
    {
        return $this->makeRequest('GET', $endPoint, $data);
    }

    /**
     * Make a POST HTTP request.
     *
     * @param string $endPoint
     * @param array $data
     *
     * @return stdClass
     */
    public function post(string $endPoint = '', array $data = []): stdClass
    {
        return $this->makeRequest('POST', $endPoint, $data);
    }

    /**
     * Make a PATCH HTTP request.
     *
     * @param string $endPoint
     * @param array $data
     *
     * @return stdClass
     */
    public function patch(string $endPoint = '', array $data = []): stdClass
    {
        return $this->makeRequest('PATCH', $endPoint, $data);
    }

    /**
     * Make a PUT HTTP request.
     *
     * @param string $endPoint
     * @param array $data
     *
     * @return stdClass
     */
    public function put(string $endPoint = '', array $data = []): stdClass
    {
        return $this->makeRequest('PUT', $endPoint, $data);
    }

    /**
     * Make a DELETE HTTP request.
     *
     * @param string $endPoint
     * @param array $data
     *
     * @return stdClass
     */
    public function delete(string $endPoint = '', array $data = []): stdClass
    {
        return $this->makeRequest('DELETE', $endPoint, $data);
    }

    /**
     * Make a HTTP request and return the response body.
     *
     * @param string $method
     * @param string $endPoint
     * @param array $data
     *
     * @return stdClass
     */
    protected function makeRequest(string $method, string $endPoint = '', array $data = []): stdClass
    {
        $dataType = in_array($method, ['POST', 'PATCH', 'PUT']) ? 'json' : 'query';
        try {

            if(!$this->config['debugging_handler']) {
                $response = $this->client->request($method, $endPoint, array_merge($this->config, [$dataType => $data]));
            } else {
                // Handler Instance
                $clientHandler = $this->client->getConfig('handler');

                // Create a middleware that echoes parts of the request.
                $tapMiddleware = Middleware::tap(function ($request) {
                    echo $request->getHeaderLine('Content-Type');
                    // application/json
                    echo $request->getBody();
                    // {"foo":"bar"}
                });

                $response = $this->client->request($method,
                    $endPoint,
                    array_merge($this->config,
                                [
                                    $dataType => $data,
                                    'handler' => $tapMiddleware($clientHandler)
                                ]
                    )
                );
            }

            return json_decode($response->getBody());
        } catch (GuzzleException $ex) {
            abort(Response::HTTP_INTERNAL_SERVER_ERROR, $ex->getMessage());
        }
    }
}