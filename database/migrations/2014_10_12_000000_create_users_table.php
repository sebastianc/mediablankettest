<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('test_lead')->default('true');
            $table->integer('aff_id')->default(241);
            $table->string('aff_password')->default('lmz2zFt2bjwbKoHsP5nX66');
            $table->string('referring_website');
            $table->integer('loan_amount');
            $table->enum('loan_purpose',['debt_consolidation', 'car_purchase', 'car_other', 'short_term_cash', 'home_improvements', 'pay_bills', 'one_off_purchase', 'special_occasion', 'other'])->nullable();
            $table->integer('loan_term');
            $table->enum('guarantor',['yes', 'no'])->nullable();
            $table->enum('title',['mr', 'mrs', 'miss', 'ms', 'dr'])->nullable();
            $table->string('first_name');
            $table->string('middle_name');
            $table->string('last_name');
            $table->enum('gender',['male', 'female'])->nullable();
            $table->string('date_of_birth');
            $table->enum('marital_status',['single', 'married', 'living_together', 'separated', 'divorced', 'widowed', 'other'])->nullable();
            $table->enum('number_of_dependents',[0, 1, 2, 3, 4, 5, 6, 7, 8, 9])->nullable();
            $table->integer('house_number');
            $table->string('house_name');
            $table->string('flat_number');
            $table->string('street_name');
            $table->string('city');
            $table->string('county');
            $table->string('post_code');
            $table->enum('residential_status',['home_owner', 'private_tenant', 'council_tenant', 'living_with_parents', 'living_with_friends', 'student_accommodation', 'other'])->nullable();
            $table->string('address_move_in_date');
            $table->string('mobile_number');
            $table->string('home_number');
            $table->string('work_number');
            $table->string('mobile_phone_type');
            $table->string('email_address');
            $table->enum('employment_status',['full_time', 'part_time', 'temporary', 'self_employed', 'unemployed', 'pension', 'disability', 'benefits', 'student'])->nullable();
            $table->enum('payment_frequency',['weekly', 'bi_weekly', 'fortnightly', 'last_working_day', 'specific_day', 'twice_monthly', 'four_weekly', 'last_friday', 'last_thursday', 'last_wednesday', 'last_tuesday', 'last_monday'])->nullable();
            $table->enum('payment_method',['uk_direct_deposit', 'non_uk_direct_deposit', 'cash', 'cheque', 'none'])->nullable();
            $table->integer('monthly_income');
            $table->string('next_pay_date');
            $table->string('following_pay_date');
            $table->string('job_title');
            $table->string('employer_name');
            $table->enum('employer_industry',['construction_manufacturing', 'military', 'health', 'banking_insurance', 'education', 'civil_service', 'supermarket_retail', 'utilities_telecom', 'hotel_restaurant_leisure', 'other_office_based', 'other_not_office_based', 'driving_delivery', 'administration_secretiarial', 'midlevel_management', 'seniorlevel_management', 'skilled_trade', 'professional', 'none'])->nullable();
            $table->string('employment_start_date');
            $table->float('expenditure_housing', 10, 2);
            $table->float('expenditure_credit', 10, 2);
            $table->float('expenditure_transport', 10, 2);
            $table->float('expenditure_food', 10, 2);
            $table->float('expenditure_utilities', 10, 2);
            $table->float('expenditure_other', 10, 2);
            $table->enum('bank_name',['bos', 'barclays', 'coop', 'firstdirect', 'halifax', 'hsbc', 'lloyds', 'nationwide', 'natwest', 'rbs', 'santander', 'tsb', 'other'])->nullable();
            $table->string('bank_account_number');
            $table->string('bank_sort_code');
            $table->enum('bank_card_type',['solo', 'switch_maestro', 'visa', 'visa_debit', 'visa_delta', 'visa_electron', 'mastercard', 'mastercard_debit'])->nullable();
            $table->boolean('consent_email_sms');
            $table->boolean('consent_email');
            $table->boolean('consent_sms');
            $table->boolean('consent_call');
            $table->boolean('consent_credit_search');
            $table->boolean('consent_financial');
            $table->string('user_agent');
            $table->string('ip_address');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
