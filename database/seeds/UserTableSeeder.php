<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    public function run()
    {
        $users = [
            0 =>
                array (
                    'aff_id' => 0,
                    'sub_id' => '5',
                    'aff_reference' => '',
                    'lead_source_id' => 230,
                    'referring_website' => 'abc.co.uk',
                    'loan_amount' => 350.0,
                    'loan_purpose' => 'bills',
                    'loan_term' => 3,
                    'guarantor' => 'no',
                    'title' => 'miss',
                    'first_name' => 'Julie',
                    'middle_name' => '',
                    'last_name' => 'Anderson',
                    'gender' => 'female',
                    'date_of_birth' => '1978-01-23',
                    'marital_status' => 'single',
                    'number_of_dependents' => 1,
                    'house_number' => '62',
                    'house_name' => '',
                    'flat_number' => '',
                    'street_name' => 'Juniper Road',
                    'city' => 'Farnborough',
                    'county' => 'Hampshire',
                    'post_code' => 'M3 9XU',
                    'residential_status' => 'private',
                    'address_move_in_date' => '25-05-2016',
                    'mobile_number' => '07707077070',
                    'home_number' => '07707077070',
                    'work_number' => '07707077070',
                    'mobile_phone_type' => 'contract',
                    'email_address' => 'test@test.com',
                    'employment_status' => 'full_time',
                    'payment_frequency' => 'last_working_day',
                    'payment_method' => 'uk_direct_deposit',
                    'monthly_income' => 2675.0,
                    'next_pay_date' => '22-11-2017',
                    'following_pay_date' => '22-12-2017',
                    'job_title' => 'Management',
                    'employer_name' => 'Sarens',
                    'employer_industry' => 'construction_manufacturing',
                    'employment_start_date' => '30-8-2015',
                    'expenditure_housing' => 334.0,
                    'expenditure_credit' => 110.0,
                    'expenditure_transport' => 60.0,
                    'expenditure_food' => 70.0,
                    'expenditure_utilities' => 75.0,
                    'expenditure_other' => 122.0,
                    'bank_name' => 'other',
                    'bank_account_number' => '10516973',
                    'bank_sort_code' => '16-00-09',
                    'bank_card_type' => 'visa_debit',
                    'consent_email_sms' => 0,
                    'consent_credit_search' => 1,
                    'consent_financial' => 1,
                    'user_agent' => 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_0_3 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A432 Safari/604.1',
                    'ip_address' => '188.223.196.40',
                ),
            1 =>
                array (
                    'aff_id' => 0,
                    'sub_id' => '691',
                    'aff_reference' => '',
                    'lead_source_id' => 229,
                    'referring_website' => 'abc.co.uk',
                    'loan_amount' => 350.0,
                    'loan_purpose' => 'car',
                    'loan_term' => 3,
                    'guarantor' => 'no',
                    'title' => 'miss',
                    'first_name' => 'Julie',
                    'middle_name' => '',
                    'last_name' => 'Anderson',
                    'gender' => 'female',
                    'date_of_birth' => '1982-09-08',
                    'marital_status' => 'single',
                    'number_of_dependents' => 0,
                    'house_number' => '62',
                    'house_name' => '',
                    'flat_number' => '',
                    'street_name' => 'Juniper Road',
                    'city' => 'Farnborough',
                    'county' => 'Hampshire',
                    'post_code' => 'M3 9XU',
                    'residential_status' => 'private',
                    'address_move_in_date' => '01-07-2016',
                    'mobile_number' => '07707077070',
                    'home_number' => '07707077070',
                    'work_number' => '07707077070',
                    'mobile_phone_type' => '',
                    'email_address' => 'test@test.com',
                    'employment_status' => 'full_time',
                    'payment_frequency' => 'specific_day',
                    'payment_method' => 'uk_direct_deposit',
                    'monthly_income' => 2675.0,
                    'next_pay_date' => '22-11-2017',
                    'following_pay_date' => '22-12-2017',
                    'job_title' => 'Construction',
                    'employer_name' => 'Sarens',
                    'employer_industry' => 'construction_manufacturing',
                    'employment_start_date' => '01-08-2015',
                    'expenditure_housing' => 535.0,
                    'expenditure_credit' => 258.0,
                    'expenditure_transport' => 125.0,
                    'expenditure_food' => 250.0,
                    'expenditure_utilities' => 159.0,
                    'expenditure_other' => 250.0,
                    'bank_name' => 'rbs',
                    'bank_account_number' => '10516973',
                    'bank_sort_code' => '16-00-09',
                    'bank_card_type' => 'visa_debit',
                    'consent_email_sms' => 0,
                    'consent_credit_search' => 1,
                    'consent_financial' => 1,
                    'user_agent' => 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_0_3 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A432 Safari/604.1',
                    'ip_address' => '188.223.196.40',
                    'logbook_loan' => 0,
                    'sold' => 0,
                    'redirect_uri' => NULL,
                    'created_at_date' => '2017-10-25',
                    'created_at' => '2017-10-25 06:11:57',
                    'updated_at' => '2017-10-25 06:11:57',
                    'our_commission' => NULL,
                    'aff_commission' => NULL,
                    'buyer_price' => NULL,
                    'redirected' => 0,
                ),
            2 =>
                array (
                    'aff_id' => 0,
                    'sub_id' => '2511276',
                    'aff_reference' => '',
                    'lead_source_id' => 229,
                    'referring_website' => 'abc.co.uk',
                    'loan_amount' => 150.0,
                    'loan_purpose' => 'short_term',
                    'loan_term' => 30,
                    'guarantor' => 'no',
                    'title' => 'Miss',
                    'first_name' => 'Julia',
                    'middle_name' => '',
                    'last_name' => 'Gudaviciute',
                    'gender' => 'female',
                    'date_of_birth' => '1990-12-01',
                    'marital_status' => 'other',
                    'number_of_dependents' => 1,
                    'house_number' => '26',
                    'house_name' => '',
                    'flat_number' => '',
                    'street_name' => 'Peacockwalk',
                    'city' => 'Crawley',
                    'county' => 'SUSSEX',
                    'post_code' => 'M3 9XU',
                    'residential_status' => 'tenant',
                    'address_move_in_date' => '25-07-2015',
                    'mobile_number' => '07707077070',
                    'home_number' => '07707077070',
                    'work_number' => '07707077070',
                    'mobile_phone_type' => '',
                    'email_address' => 'test@test.com',
                    'employment_status' => 'full_time',
                    'payment_frequency' => 'last_working_day',
                    'payment_method' => 'uk_direct_deposit',
                    'monthly_income' => 1750.0,
                    'next_pay_date' => '31-10-2017',
                    'following_pay_date' => '30-11-2017',
                    'job_title' => 'Employed',
                    'employer_name' => 'Thomascook',
                    'employer_industry' => 'other_office_based',
                    'employment_start_date' => '25-07-2015',
                    'expenditure_housing' => 497.0,
                    'expenditure_credit' => 224.0,
                    'expenditure_transport' => 149.0,
                    'expenditure_food' => 186.0,
                    'expenditure_utilities' => 112.0,
                    'expenditure_other' => 10.0,
                    'bank_name' => 'hsbc',
                    'bank_account_number' => '51697536',
                    'bank_sort_code' => '40-44-20',
                    'bank_card_type' => 'visa_debit',
                    'consent_email_sms' => 0,
                    'consent_credit_search' => 1,
                    'consent_financial' => 1,
                    'user_agent' => 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_0_3 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A432 Safari/604.1',
                    'ip_address' => '94.174.102.192',
                ),
            3 =>
                array (
                    'aff_id' => 130,
                    'sub_id' => '101',
                    'aff_reference' => '',
                    'lead_source_id' => 230,
                    'referring_website' => 'abc.co.uk',
                    'loan_amount' => 150.0,
                    'loan_purpose' => 'bills',
                    'loan_term' => 1,
                    'guarantor' => 'no',
                    'title' => 'mr',
                    'first_name' => 'Melvyn',
                    'middle_name' => '',
                    'last_name' => 'Gayle',
                    'gender' => 'male',
                    'date_of_birth' => '1980-10-12',
                    'marital_status' => 'single',
                    'number_of_dependents' => 2,
                    'house_number' => '',
                    'house_name' => 'Apartment 18, 21-25',
                    'flat_number' => '',
                    'street_name' => 'Ashton Lane',
                    'city' => 'Sale',
                    'county' => 'Greater Manchester',
                    'post_code' => 'M3 9XU',
                    'residential_status' => 'private',
                    'address_move_in_date' => '25-05-2015',
                    'mobile_number' => '07707077070',
                    'home_number' => '07707077070',
                    'work_number' => '07707077070',
                    'mobile_phone_type' => 'contract',
                    'email_address' => 'test@test.com',
                    'employment_status' => 'full_time',
                    'payment_frequency' => 'last_working_day',
                    'payment_method' => 'uk_direct_deposit',
                    'monthly_income' => 1500.0,
                    'next_pay_date' => '31-10-2017',
                    'following_pay_date' => '30-11-2017',
                    'job_title' => 'Administrator',
                    'employer_name' => 'Adt',
                    'employer_industry' => 'other_office_based',
                    'employment_start_date' => '19-5-2014',
                    'expenditure_housing' => 200.0,
                    'expenditure_credit' => 120.0,
                    'expenditure_transport' => 30.0,
                    'expenditure_food' => 60.0,
                    'expenditure_utilities' => 50.0,
                    'expenditure_other' => 68.0,
                    'bank_name' => 'other',
                    'bank_account_number' => '00072724',
                    'bank_sort_code' => '11-06-68',
                    'bank_card_type' => 'visa_debit',
                    'consent_email_sms' => 0,
                    'consent_email' => 0,
                    'consent_call' => 0,
                    'consent_sms' => 0,
                    'consent_credit_search' => 1,
                    'consent_financial' => 1,
                    'user_agent' => 'Mozilla/5.0 (Linux; Android 5.0.1; SAMSUNG GT-I9505 Build/LRX22C) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/5.4 Chrome/51.0.2704.106 Mobile Safari/537.36',
                    'ip_address' => '109.144.219.152',
                ),
            4 =>
                array (
                    'aff_id' => 122,
                    'sub_id' => '153',
                    'aff_reference' => '',
                    'lead_source_id' => 229,
                    'referring_website' => 'abc.co.uk',
                    'loan_amount' => 150.0,
                    'loan_purpose' => 'short_term',
                    'loan_term' => 1,
                    'guarantor' => 'no',
                    'title' => 'mr',
                    'first_name' => 'Melvyn',
                    'middle_name' => '',
                    'last_name' => 'Gayle',
                    'gender' => 'male',
                    'date_of_birth' => '1965-01-23',
                    'marital_status' => 'single',
                    'number_of_dependents' => 1,
                    'house_number' => '',
                    'house_name' => 'Apartment 18, 21-25',
                    'flat_number' => '',
                    'street_name' => 'Ashton Lane',
                    'city' => 'Sale',
                    'county' => 'Greater Manchester',
                    'post_code' => 'M3 9XU',
                    'residential_status' => 'tenant',
                    'address_move_in_date' => '25-10-2015',
                    'mobile_number' => '07707077070',
                    'home_number' => '07707077070',
                    'work_number' => '07707077070',
                    'mobile_phone_type' => '',
                    'email_address' => 'test@test.com',
                    'employment_status' => 'full_time',
                    'payment_frequency' => 'last_working_day',
                    'payment_method' => 'uk_direct_deposit',
                    'monthly_income' => 1500.0,
                    'next_pay_date' => '31-10-2017',
                    'following_pay_date' => '30-11-2017',
                    'job_title' => 'Customer service advisor',
                    'employer_name' => 'Adt',
                    'employer_industry' => 'other_office_based',
                    'employment_start_date' => '25-10-2009',
                    'expenditure_housing' => 400.0,
                    'expenditure_credit' => 100.0,
                    'expenditure_transport' => 30.0,
                    'expenditure_food' => 80.0,
                    'expenditure_utilities' => 150.0,
                    'expenditure_other' => 0.0,
                    'bank_name' => 'bos',
                    'bank_account_number' => '00072724',
                    'bank_sort_code' => '11-06-68',
                    'bank_card_type' => 'visa_debit',
                    'consent_email_sms' => 0,
                    'consent_email' => 1,
                    'consent_call' => 0,
                    'consent_sms' => 1,
                    'consent_credit_search' => 1,
                    'consent_financial' => 1,
                    'user_agent' => 'Mozilla/5.0 (Linux; Android 5.0.1; SAMSUNG GT-I9505 Build/LRX22C) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/5.4 Chrome/51.0.2704.106 Mobile Safari/537.36',
                    'ip_address' => '109.144.219.152',
                ),
            5 =>
                array (
                    'aff_id' => 132,
                    'sub_id' => '721',
                    'aff_reference' => '',
                    'lead_source_id' => 229,
                    'referring_website' => 'abc.co.uk',
                    'loan_amount' => 150.0,
                    'loan_purpose' => 'bills',
                    'loan_term' => 1,
                    'guarantor' => 'no',
                    'title' => 'dr',
                    'first_name' => 'Mark',
                    'middle_name' => '',
                    'last_name' => 'Desmond',
                    'gender' => 'male',
                    'date_of_birth' => '1986-05-05',
                    'marital_status' => 'single',
                    'number_of_dependents' => 1,
                    'house_number' => '',
                    'house_name' => '',
                    'flat_number' => '7 berryside apartments',
                    'street_name' => 'Swan Lane',
                    'city' => 'Hackney',
                    'county' => 'London',
                    'post_code' => 'M3 9XU',
                    'residential_status' => 'parents',
                    'address_move_in_date' => '01-07-2015',
                    'mobile_number' => '07707077070',
                    'home_number' => '07707077070',
                    'work_number' => '07707077070',
                    'mobile_phone_type' => '',
                    'email_address' => 'test@test.com',
                    'employment_status' => 'self_employed',
                    'payment_frequency' => 'weekly',
                    'payment_method' => 'uk_direct_deposit',
                    'monthly_income' => 1300.0,
                    'next_pay_date' => '26-10-2017',
                    'following_pay_date' => '02-11-2017',
                    'job_title' => 'Construction',
                    'employer_name' => 'Mark Desmond',
                    'employer_industry' => 'construction_manufacturing',
                    'employment_start_date' => '01-09-2013',
                    'expenditure_housing' => 0.0,
                    'expenditure_credit' => 0.0,
                    'expenditure_transport' => 50.0,
                    'expenditure_food' => 250.0,
                    'expenditure_utilities' => 80.0,
                    'expenditure_other' => 0.0,
                    'bank_name' => 'nationwide',
                    'bank_account_number' => '37006885',
                    'bank_sort_code' => '07-02-46',
                    'bank_card_type' => 'visa_debit',
                    'consent_email_sms' => 0,
                    'consent_email' => 0,
                    'consent_call' => 1,
                    'consent_sms' => 1,
                    'consent_credit_search' => 1,
                    'consent_financial' => 1,
                    'user_agent' => 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_3 like Mac OS X) AppleWebKit/603.3.8 (KHTML, like Gecko) Version/10.0 Mobile/14G60 Safari/602.1',
                    'ip_address' => '85.255.234.16',
                ),
            6 =>
                array (
                    'aff_id' => 111,
                    'sub_id' => '',
                    'aff_reference' => '',
                    'lead_source_id' => 230,
                    'referring_website' => 'abc.co.uk',
                    'loan_amount' => 150.0,
                    'loan_purpose' => 'short_term',
                    'loan_term' => 1,
                    'guarantor' => 'yes',
                    'title' => 'mr',
                    'first_name' => 'Mark',
                    'middle_name' => '',
                    'last_name' => 'Desmond',
                    'gender' => 'female',
                    'date_of_birth' => '1987-12-06',
                    'marital_status' => 'single',
                    'number_of_dependents' => 1,
                    'house_number' => '7',
                    'house_name' => '',
                    'flat_number' => '',
                    'street_name' => 'Swan Lane',
                    'city' => 'Hackney',
                    'county' => 'London',
                    'post_code' => 'M3 9XU',
                    'residential_status' => 'parents',
                    'address_move_in_date' => '25-07-2015',
                    'mobile_number' => '07707077070',
                    'home_number' => '07707077070',
                    'work_number' => '07707077070',
                    'mobile_phone_type' => '',
                    'email_address' => 'test@test.com',
                    'employment_status' => 'self_employed',
                    'payment_frequency' => 'weekly',
                    'payment_method' => 'uk_direct_deposit',
                    'monthly_income' => 1300.0,
                    'next_pay_date' => '26-10-2017',
                    'following_pay_date' => '02-11-2017',
                    'job_title' => 'Other',
                    'employer_name' => 'Mark Desmond',
                    'employer_industry' => 'construction_manufacturing',
                    'employment_start_date' => '25-09-2013',
                    'expenditure_housing' => 0.0,
                    'expenditure_credit' => 0.0,
                    'expenditure_transport' => 50.0,
                    'expenditure_food' => 250.0,
                    'expenditure_utilities' => 80.0,
                    'expenditure_other' => 0.0,
                    'bank_name' => 'other',
                    'bank_account_number' => '37006885',
                    'bank_sort_code' => '07-02-46',
                    'bank_card_type' => 'visa_debit',
                    'consent_email_sms' => 1,
                    'consent_email' => 0,
                    'consent_call' => 1,
                    'consent_sms' => 1,
                    'consent_credit_search' => 1,
                    'consent_financial' => 1,
                    'user_agent' => 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_3 like Mac OS X) AppleWebKit/603.3.8 (KHTML, like Gecko) Version/10.0 Mobile/14G60 Safari/602.1',
                    'ip_address' => '85.255.234.16',
                ),
            7 =>
                array (
                    'aff_id' => 132,
                    'sub_id' => '700',
                    'aff_reference' => '',
                    'lead_source_id' => 229,
                    'referring_website' => 'abc.co.uk',
                    'loan_amount' => 100.0,
                    'loan_purpose' => 'special',
                    'loan_term' => 1,
                    'guarantor' => 'no',
                    'title' => 'mr',
                    'first_name' => 'Connor',
                    'middle_name' => '',
                    'last_name' => 'Davies',
                    'gender' => 'male',
                    'date_of_birth' => '1997-01-01',
                    'marital_status' => 'single',
                    'number_of_dependents' => 0,
                    'house_number' => '86',
                    'house_name' => '',
                    'flat_number' => '',
                    'street_name' => 'Moncrieff Way',
                    'city' => 'Livingston',
                    'county' => 'West Lothian',
                    'post_code' => 'M3 9XU',
                    'residential_status' => 'parents',
                    'address_move_in_date' => '01-05-2008',
                    'mobile_number' => '07707077070',
                    'home_number' => '07707077070',
                    'work_number' => '07707077070',
                    'mobile_phone_type' => '',
                    'email_address' => 'test@test.com',
                    'employment_status' => 'full_time',
                    'payment_frequency' => 'last_friday',
                    'payment_method' => 'uk_direct_deposit',
                    'monthly_income' => 1400.0,
                    'next_pay_date' => '27-10-2017',
                    'following_pay_date' => '24-11-2017',
                    'job_title' => 'Other',
                    'employer_name' => 'Blue Arrow',
                    'employer_industry' => 'other_not_office_based',
                    'employment_start_date' => '01-04-2015',
                    'expenditure_housing' => 0.0,
                    'expenditure_credit' => 0.0,
                    'expenditure_transport' => 25.0,
                    'expenditure_food' => 25.0,
                    'expenditure_utilities' => 0.0,
                    'expenditure_other' => 0.0,
                    'bank_name' => 'other',
                    'bank_account_number' => '10877166',
                    'bank_sort_code' => '83-28-45',
                    'bank_card_type' => 'visa_debit',
                    'consent_email_sms' => 0,
                    'consent_email' => 1,
                    'consent_call' => 1,
                    'consent_sms' => 1,
                    'consent_credit_search' => 1,
                    'consent_financial' => 1,
                    'user_agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36',
                    'ip_address' => '82.27.78.219',
                ),
            8 =>
                array (
                    'aff_id' => 130,
                    'sub_id' => '5',
                    'aff_reference' => '',
                    'lead_source_id' => 230,
                    'referring_website' => 'abc.co.uk',
                    'loan_amount' => 150.0,
                    'loan_purpose' => 'car',
                    'loan_term' => 1,
                    'guarantor' => 'no',
                    'title' => 'mr',
                    'first_name' => 'Mark',
                    'middle_name' => '',
                    'last_name' => 'Desmond',
                    'gender' => 'male',
                    'date_of_birth' => '1978-01-09',
                    'marital_status' => 'single',
                    'number_of_dependents' => 0,
                    'house_number' => '',
                    'house_name' => '7 Berryside Apartments',
                    'flat_number' => '',
                    'street_name' => 'Swan Lane',
                    'city' => 'Hackney',
                    'county' => 'London',
                    'post_code' => 'M3 9XU',
                    'residential_status' => 'parents',
                    'address_move_in_date' => '25-05-2015',
                    'mobile_number' => '07707077070',
                    'home_number' => '07707077070',
                    'work_number' => '07707077070',
                    'mobile_phone_type' => 'contract',
                    'email_address' => 'test@test.com',
                    'employment_status' => 'self_employed',
                    'payment_frequency' => 'last_working_day',
                    'payment_method' => 'uk_direct_deposit',
                    'monthly_income' => 1300.0,
                    'next_pay_date' => '26-10-2017',
                    'following_pay_date' => '02-11-2017',
                    'job_title' => 'Representative',
                    'employer_name' => 'Mark Desmond',
                    'employer_industry' => 'construction_manufacturing',
                    'employment_start_date' => '17-7-2015',
                    'expenditure_housing' => 200.0,
                    'expenditure_credit' => 110.0,
                    'expenditure_transport' => 30.0,
                    'expenditure_food' => 70.0,
                    'expenditure_utilities' => 20.0,
                    'expenditure_other' => 59.0,
                    'bank_name' => 'other',
                    'bank_account_number' => '37006885',
                    'bank_sort_code' => '07-02-46',
                    'bank_card_type' => 'visa_debit',
                    'consent_email_sms' => 0,
                    'consent_email' => 1,
                    'consent_call' => 0,
                    'consent_sms' => 0,
                    'consent_credit_search' => 1,
                    'consent_financial' => 1,
                    'user_agent' => 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_3 like Mac OS X) AppleWebKit/603.3.8 (KHTML, like Gecko) Version/10.0 Mobile/14G60 Safari/602.1',
                    'ip_address' => '85.255.234.16',
                ),
            9 =>
                array (
                    'aff_id' => 132,
                    'sub_id' => '12268',
                    'aff_reference' => '',
                    'lead_source_id' => 229,
                    'referring_website' => 'abc.co.uk',
                    'loan_amount' => 500.0,
                    'loan_purpose' => 'bills',
                    'loan_term' => 3,
                    'guarantor' => 'no',
                    'title' => 'mrs',
                    'first_name' => 'Suzanne',
                    'middle_name' => '',
                    'last_name' => 'Connor',
                    'gender' => 'female',
                    'date_of_birth' => '1964-10-10',
                    'marital_status' => 'single',
                    'number_of_dependents' => 0,
                    'house_number' => '22',
                    'house_name' => '',
                    'flat_number' => '',
                    'street_name' => 'Glenview Drive',
                    'city' => 'Falkirk',
                    'county' => 'Stirlingshire',
                    'post_code' => 'M3 9XU',
                    'residential_status' => 'owner',
                    'address_move_in_date' => '01-02-2007',
                    'mobile_number' => '07707077070',
                    'home_number' => '07707077070',
                    'work_number' => '07707077070',
                    'mobile_phone_type' => '',
                    'email_address' => 'test@test.com',
                    'employment_status' => 'full_time',
                    'payment_frequency' => 'last_thursday',
                    'payment_method' => 'uk_direct_deposit',
                    'monthly_income' => 2900.0,
                    'next_pay_date' => '26-10-2017',
                    'following_pay_date' => '30-11-2017',
                    'job_title' => 'Health',
                    'employer_name' => 'NHS Forth Valley',
                    'employer_industry' => 'health',
                    'employment_start_date' => '01-07-1983',
                    'expenditure_housing' => 348.0,
                    'expenditure_credit' => 900.0,
                    'expenditure_transport' => 150.0,
                    'expenditure_food' => 200.0,
                    'expenditure_utilities' => 150.0,
                    'expenditure_other' => 100.0,
                    'bank_name' => 'barclays',
                    'bank_account_number' => '50497088',
                    'bank_sort_code' => '20-70-94',
                    'bank_card_type' => 'visa_debit',
                    'consent_email_sms' => 0,
                    'consent_credit_search' => 1,
                    'consent_email' => 1,
                    'consent_call' => 0,
                    'consent_sms' => 1,
                    'consent_financial' => 1,
                    'user_agent' => 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_3_5 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13G36 Safari/601.1',
                    'ip_address' => '82.37.210.204',
                ),
        ];
        App\User::truncate();

        foreach ($users as $user) {
            $newUser = new \App\User;

            $newUser->mobile_number = $user['mobile_number'];
            $newUser->home_number = $user['home_number'];
            $newUser->work_number = $user['work_number'];
            $newUser->email_address = strtolower($user['email_address']);
            $newUser->date_of_birth = $user['date_of_birth'];
            $newUser->address_move_in_date = $user['address_move_in_date'];
            $newUser->next_pay_date = $user['next_pay_date'];
            $newUser->following_pay_date = $user['following_pay_date'];
            $newUser->employment_start_date = $user['employment_start_date'];

            if(!isset($user['consent_email'])){
                $user['consent_email'] = false;
            }
            if(!isset($user['consent_sms'])){
                $user['consent_sms'] = false;
            }
            if(!isset($user['consent_call'])){
                $user['consent_call'] = false;
            }

            if(!in_array(strtolower($user['loan_purpose']), ['debt_consolidation', 'car_purchase', 'car_other', 'short_term_cash', 'home_improvements', 'pay_bills', 'one_off_purchase', 'special_occasion', 'other'])){
                unset($user['loan_purpose']);
            }
            else {
                $newUser->loan_purpose = strtolower($user['loan_purpose']);
            }

            if(!in_array(strtolower($user['guarantor']), ['yes', 'no'])){
                unset($user['guarantor']);
            }
            else {
                $newUser->guarantor = strtolower($user['guarantor']);
            }

            if(!in_array(strtolower($user['title']), ['mr', 'mrs', 'miss', 'ms', 'dr'])){
                unset($user['title']);
            }
            else {
                $newUser->title = strtolower($user['title']);
            }

            if(!in_array(strtolower($user['gender']), ['male', 'female'])){
                unset($user['gender']);
            }
            else {
                $newUser->gender = strtolower($user['gender']);
            }

            if(!in_array(strtolower($user['marital_status']), ['single', 'married', 'living_together', 'separated', 'divorced', 'widowed', 'other'])){
                unset($user['marital_status']);
            }
            else {
                $newUser->marital_status = strtolower($user['marital_status']);
            }

            if(!isset($user['number_of_dependents'])) {
                $user['number_of_dependents'] = 0;
            } elseif (!in_array($user['number_of_dependents'], [0, 1, 2, 3, 4, 5, 6, 7, 8, 9])){
                if(is_int($user['number_of_dependents'])) {
                    if($user['number_of_dependents'] > 9) {

                        $user['number_of_dependents'] = 9;
                        $newUser->number_of_dependents = $user['number_of_dependents'];

                    } elseif ($user['number_of_dependents'] < 0 && $user['number_of_dependents'] > -10) {

                        $newUser->number_of_dependents = abs($user['number_of_dependents']);

                    } elseif($user['number_of_dependents'] < -10){

                        $user['number_of_dependents'] = 9;
                        $newUser->number_of_dependents = $user['number_of_dependents'];

                    }
                } else {
                    unset($user['number_of_dependents']);
                }
            } else {
                $newUser->number_of_dependents = $user['number_of_dependents'];
            }

            if(!in_array(strtolower($user['residential_status']), ['home_owner', 'private_tenant', 'council_tenant', 'living_with_parents', 'living_with_friends', 'student_accommodation', 'other'])){
                unset($user['residential_status']);
            }
            else {
                $newUser->residential_status = strtolower($user['residential_status']);
            }

            if(!in_array(strtolower($user['employment_status']), ['full_time', 'part_time', 'temporary', 'self_employed', 'unemployed', 'pension', 'disability', 'benefits', 'student'])){
                unset($user['employment_status']);
            }
            else {
                $newUser->employment_status = strtolower($user['employment_status']);
            }

            if(!in_array(strtolower($user['payment_frequency']), ['weekly', 'bi_weekly', 'fortnightly', 'last_working_day', 'specific_day', 'twice_monthly', 'four_weekly', 'last_friday', 'last_thursday', 'last_wednesday', 'last_tuesday', 'last_monday'])){
                unset($user['payment_frequency']);
            }
            else {
                $newUser->payment_frequency = strtolower($user['payment_frequency']);
            }

            if(!in_array(strtolower($user['payment_method']), ['uk_direct_deposit', 'non_uk_direct_deposit', 'cash', 'cheque', 'none'])){
                unset($user['payment_method']);
            }
            else {
                $newUser->payment_method = strtolower($user['payment_method']);
            }

            if(!in_array(strtolower($user['employer_industry']), ['construction_manufacturing', 'military', 'health', 'banking_insurance', 'education', 'civil_service', 'supermarket_retail', 'utilities_telecom', 'hotel_restaurant_leisure', 'other_office_based', 'other_not_office_based', 'driving_delivery', 'administration_secretiarial', 'midlevel_management', 'seniorlevel_management', 'skilled_trade', 'professional', 'none'])){
                unset($user['employer_industry']);
            }
            else {
                $newUser->employer_industry = strtolower($user['employer_industry']);
            }

            if(!in_array(strtolower($user['bank_name']), ['bos', 'barclays', 'coop', 'firstdirect', 'halifax', 'hsbc', 'lloyds', 'nationwide', 'natwest', 'rbs', 'santander', 'tsb', 'other'])){
                unset($user['bank_name']);
            }
            else {
                $newUser->bank_name = strtolower($user['bank_name']);
            }

            if(!in_array(strtolower($user['bank_card_type']), ['solo', 'switch_maestro', 'visa', 'visa_debit', 'visa_delta', 'visa_electron', 'mastercard', 'mastercard_debit'])){
                unset($user['bank_card_type']);
            }
            else {
                $newUser->bank_card_type = strtolower($user['bank_card_type']);
            }


            // Seeder calls the rest of the validation functions in the model

            if($newUser->isMobileNumber()
                && $newUser->isUkMobile()
                && $newUser->isUkHome()
                && $newUser->isUkWork()
                && $newUser->isValidEmail()
                && $newUser->isDateOfBirthRight()
                && $newUser->isAddressMovingDateRight()
                && $newUser->isNextPayDateRight()
                && $newUser->isFollowingPayDateRight()
                && $newUser->isEmploymentStartDateRight()) {
                $newUser->first_name = 'Sebastian';
                $newUser->last_name = 'Ciocan';
                $newUser->test_lead = true;
                $newUser->aff_id = 241;
                $newUser->aff_password = 'lmz2zFt2bjwbKoHsP5nX66';
                unset($user['sub_id']);
                $newUser->referring_website = $user['referring_website'];
                $newUser->loan_amount = $user['loan_amount'];
                $newUser->loan_term = $user['loan_term'];
                $newUser->middle_name = $user['middle_name'];
                $newUser->house_number = $user['house_number'];
                $newUser->house_name = $user['house_name'];
                $newUser->flat_number = $user['flat_number'];
                $newUser->street_name = $user['street_name'];
                $newUser->city = $user['city'];
                $newUser->county = $user['county'];
                $newUser->post_code = $user['post_code'];
                $newUser->mobile_phone_type = $user['mobile_phone_type'];
                $newUser->monthly_income = (int)$user['monthly_income'];
                $newUser->job_title = $user['job_title'];
                $newUser->employer_name = $user['employer_name'];
                $newUser->expenditure_housing = $user['expenditure_housing'];
                $newUser->expenditure_credit = $user['expenditure_credit'];
                $newUser->expenditure_transport = $user['expenditure_transport'];
                $newUser->expenditure_food = $user['expenditure_food'];
                $newUser->expenditure_utilities = $user['expenditure_utilities'];
                $newUser->expenditure_other = $user['expenditure_other'];
                $newUser->bank_account_number = $user['bank_account_number'];
                $newUser->bank_sort_code = $user['bank_sort_code'];
                $newUser->consent_email_sms = $user['consent_email_sms'];
                $newUser->consent_email = $user['consent_email'];
                $newUser->consent_sms = $user['consent_sms'];
                $newUser->consent_call = $user['consent_call'];
                $newUser->consent_credit_search = $user['consent_credit_search'];
                $newUser->consent_financial = $user['consent_financial'];
                $newUser->user_agent = $user['user_agent'];
                $newUser->ip_address = $user['ip_address'];
                $newUser->save();
            } else {
                unset($newUser);
                continue;
            }
        }

    }
}